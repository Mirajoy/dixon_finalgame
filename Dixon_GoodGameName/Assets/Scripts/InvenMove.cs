﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvenMove : MonoBehaviour {

    public Canvas Inventory;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClick()
    {
        if (Inventory.enabled == false)
        {
            Inventory.enabled = true;
        }
        else
        {
            Inventory.enabled = false;
        }
    }
}
