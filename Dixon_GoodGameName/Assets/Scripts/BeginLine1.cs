﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BeginLine1 : MonoBehaviour {

    public Text Line1;
    public Text Line2;
    public Image Pic;
    public GameObject BeginBtn;
    public GameObject NextBtn;
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClick()
    {
        Line1.enabled = false;
        NextBtn.SetActive(false);
        Line2.enabled = true;
        Pic.enabled = true;
        BeginBtn.SetActive(true);
    }
}
