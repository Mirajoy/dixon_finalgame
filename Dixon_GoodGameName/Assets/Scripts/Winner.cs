﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Winner : MonoBehaviour {

    public GameObject Item1;
    public GameObject Item2;
    public GameObject Item3;
    public GameObject Item4;
    public GameObject Item5;
    public GameObject Item6;
    //Creates slots for all of the items needed to win
    public Canvas WinScene; 
    //Calls the win screen

    

    // Use this for initialization
    void Start () {

        
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    //If all of the items are active in the inventory, show win screen
    public void OnClick()
    {
        if (Item1.activeInHierarchy == true && Item2.activeInHierarchy == true && Item3.activeInHierarchy == true && Item4.activeInHierarchy == true && Item5.activeInHierarchy == true && Item6.activeInHierarchy == true)
        {
            WinScene.enabled = true;
        }
    }
}
