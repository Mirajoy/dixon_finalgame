﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class Items : MonoBehaviour {

    public GameObject Weapon;
    public string InInv;
    public GameObject InvWeapon;

	// Use this for initialization
	void Start () {	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClick()
    {
        Weapon.SetActive(false);
        InvWeapon.SetActive(true);
    }
}